﻿using UnityEngine;
using System.Collections;


public class TileTextureCollection : MonoBehaviour {
	public static TileTextureCollection ActiveCollection { get; private set; }
	
	public Texture[] textures;
	
	void Awake(){
		ActiveCollection = this;	
	}
}
