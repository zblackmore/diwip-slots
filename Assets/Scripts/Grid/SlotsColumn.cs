﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;

namespace DiwipSlots
{
	public class SlotsColumn : MonoBehaviour
	{
		SlotsGridDef gridDef;
		List<Tile> tiles;
		
		int tilesSpinned;
		int onScreenTilesCount;
		
		public List<int> FinalStateImageIndexes { get; set; }
		
		public void InitWithGridDef(SlotsGridDef def){
			gridDef = def;
		}
		
		void Start ()
		{
			tiles = new List<Tile> ();
			foreach (Transform child in transform) {
				tiles.Add (child.GetComponent<Tile>());
			}			
			tiles = tiles.OrderByDescending(t => t.transform.position.y).ToList();
			FinalStateImageIndexes = new List<int>() { 2, 2, 2};
			onScreenTilesCount = tiles.Count - gridDef.paddingRows;
			
		}
	
		// Update is called once per frame
		void Update ()
		{
			Tile bottomTile = tiles[tiles.Count - 1];
			Transform bottomTileTransform = bottomTile.transform;
			
			if (bottomTileTransform.position.y <= gridDef.bounds.y - gridDef.bounds.height - gridDef.yBetweenTiles){
				Vector3 newPosition = bottomTileTransform.position;
				newPosition.y = tiles[0].transform.position.y + gridDef.yBetweenTiles;
				bottomTileTransform.position = newPosition;
				tiles.Remove(bottomTile);
				tiles.Insert(0, bottomTile);
				int tilesToGo = Constants.SPIN_TILES_COUNT - tilesSpinned;
				tilesSpinned ++;
				int tileFinalPosition = tilesToGo - gridDef.paddingRows;
				if (tileFinalPosition > 0 && tileFinalPosition <= onScreenTilesCount){
					int finalTileIndexImage = FinalStateImageIndexes[tileFinalPosition - 1];
					bottomTile.ImageIndex = finalTileIndexImage;	
				}
				else{
					bottomTile.SetRandomImage();
				}				
				
			}
		}
		
		public void SpinFinished(){
			var tilePositions = tiles.Select(t => t.transform.position).ToList();
			Vector3 pos = transform.position;
			pos.y = 0;
			transform.position = pos;
			for (int i = 0; i < tiles.Count; i ++){
				tiles[i].transform.position = tilePositions[i];
				tilesSpinned = 0;
			}
			tiles.ForEach(t => t.Highlighted = false);
		}
		
		public void Highlight(int y){
			tiles[y + gridDef.paddingRows].Highlighted = true;
		}
	}
}