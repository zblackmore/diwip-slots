﻿using UnityEngine;
using System.Collections;

namespace DiwipSlots
{
	public class Tile : MonoBehaviour
	{
	
		int imageIndex;

		public int ImageIndex {
			get {
				return imageIndex;	
			}
			set {
				if (value != imageIndex){
					imageIndex = value;
					mat.mainTexture = TileTextureCollection.ActiveCollection.textures[imageIndex];
				}
			}
		}
		
		GameObject highlight;
		bool highlighted;
		public bool Highlighted {
			get {
				return highlighted;
			}
			
			set {
				if (value != highlighted){
					highlighted = value;
					if (highlighted) {
						startFlashing();
					}
					else {				
						stopFlashing();
					}	
				}
			}
		}
		
		void startFlashing(){
						highlight.SetActive(true);
			Color col = highlight.renderer.material.color;
			col.a = 0;
			highlight.renderer.material.color = col;
			iTween.Stop(highlight);
			iTween.FadeTo(highlight, 1, 0.5f);
			iTween.FadeTo(highlight, iTween.Hash("time", 0.5f, "alpha", 1 , "loopType", "pingPong"));
		}
		
		void stopFlashing(){
			iTween.Stop(highlight);		
			highlight.SetActive(false);			
		}
		
		GameObject content;
		Material mat;
		
		void Awake(){
			content = transform.GetChild(0).FindChild("Content").gameObject;
			mat = content.GetComponent<Renderer>().material;
			highlight = transform.GetChild(0).FindChild("Highlight").gameObject;
		}
		
		public void SetRandomImage(){
			ImageIndex = Random.Range(0, TileTextureCollection.ActiveCollection.textures.Length);
		}
	}
	
}