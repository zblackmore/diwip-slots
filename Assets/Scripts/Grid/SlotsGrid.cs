﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace DiwipSlots
{
	public struct SlotsGridDef
	{
		public List<SlotsColumn> columns;
		public int rowCount;
		public int paddingRows;
		public Rect bounds;
		public float xBetweenTiles;
		public float yBetweenTiles;
	}

	public class SlotsGrid : MonoBehaviour
	{
		SlotsGridDef def;
		int spinningColumnsCount;
		
		public event System.Action OnSpinFinished;
	
		public void Init (SlotsGridDef def)
		{
			this.def = def;
		}
	
		public void Spin (int[][] finalState)
		{			
			if (spinningColumnsCount == 0) {
				spinningColumnsCount = def.columns.Count;
				for (int i = 0; i < def.columns.Count; i ++) {
					def.columns[i].SpinFinished();
					def.columns[i].FinalStateImageIndexes = new List<int>(finalState[i]);
					float delay = .07f * i;
					iTween.MoveBy (def.columns [i].gameObject, iTween.Hash ("y", -def.yBetweenTiles * Constants.SPIN_TILES_COUNT, 
						"easeType", "easeInOutCubic", 
						"delay", delay,
						"speed", 1000 - 100 * i, 
						"oncomplete", "onTweenComplete", 
						"oncompletetarget", gameObject,
						"oncompleteparams", i));
				}
			}
		}
	
		void onTweenComplete (int columnIndex)
		{
			spinningColumnsCount --;
			if (spinningColumnsCount == 0){
				TimerManager.ActiveTimer.FireTimeTimer(0.2f, (timer) => OnSpinFinished());
			}
		}	
		
		public void HighlightTiles(Vector2[] tileIndices){
			foreach (Vector2 index in tileIndices){
				def.columns[(int)index.x].Highlight((int)index.y);
			}
		}
	}
	
}