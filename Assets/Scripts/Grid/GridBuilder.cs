﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace DiwipSlots
{
	public class GridBuilder : MonoBehaviour
	{
	
		public int rowCount;
		public int columnCount;
		public GameObject tilePrefab;
		Vector3 tileScale;
		GameObject gridObject;
		List<SlotsColumn> columns;
		float leftX;
		float bottomY;
		float spaceBetweenTiles;
		SlotsGridDef gridDef;
		const int PADDING_ROWS = 3;
	
		// Use this for initialization
		void Awake ()
		{
			spaceBetweenTiles = tilePrefab.transform.localScale.x;
				
			leftX = -(columnCount - 1) * 0.5f * spaceBetweenTiles;
			bottomY = -(rowCount - 1) * 0.5f * spaceBetweenTiles;
			
			float gridWidth = spaceBetweenTiles * columnCount;
			float gridHeight = spaceBetweenTiles * rowCount;
				
			gridDef = new SlotsGridDef() { rowCount = rowCount, paddingRows = PADDING_ROWS, bounds = new Rect(leftX, bottomY + gridHeight, gridWidth, gridHeight), xBetweenTiles = spaceBetweenTiles, yBetweenTiles = spaceBetweenTiles};
		
			gridObject = new GameObject ("Grid");
			gridObject.transform.parent = transform.parent;
		
			for (int column = 0; column < columnCount; column ++) {
				createColumn (column);
			}
			
			gridDef.columns = columns;
		
			for (int column = 0; column < columnCount; column ++) {
				for (int row = 0; row < rowCount + PADDING_ROWS; row ++) {
					createTile (row, column);
				}
			}
			
			gridObject.AddComponent<SlotsGrid>().Init(gridDef);
			Destroy(this);		
		}
	
		GameObject createTile (int row, int column)
		{
			GameObject tile = (GameObject)GameObject.Instantiate (tilePrefab);
			if (gridObject == null)
				Debug.Log ("123");
			tile.transform.parent = columns [column].transform;
			tile.name = "Tile (" + column + ", " + row + ")";
			Vector3 position = Vector3.zero;
			position.y = bottomY + row * spaceBetweenTiles;
			tile.transform.localPosition = position;
			tile.GetComponent<Tile> ().SetRandomImage ();
			return tile;
		}
	
		GameObject createColumn (int columnIndex)
		{	
			if (columns == null) {
				columns = new List<SlotsColumn> ();
			}
		
			GameObject column = new GameObject ("Column " + columnIndex);
			column.transform.parent = gridObject.transform;
			float tileX = leftX + columnIndex * spaceBetweenTiles;
			column.transform.position = new Vector3 (tileX, 0, 0); 
			SlotsColumn colComp = column.AddComponent<SlotsColumn>();
			colComp.InitWithGridDef(gridDef);
			columns.Add (colComp);
			
			
			return column;
		} 
	
		// Update is called once per frame
		void Update ()
		{
	
		}
	}
	
}