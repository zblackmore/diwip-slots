﻿using UnityEngine;
using System.Collections;

namespace DiwipSlots {

	public class GameLogic : MonoBehaviour {
		public int Coins { get; private set; }
		SlotsGrid grid;
		WinStateCheck currentResultCheck;
		
		bool spinning;
		
		public bool Spinning { get { return spinning; } }
		
		public event System.Action<WinStateCheck> OnWin;
		
		
		void Start() {
			Coins = 100;
			grid = GameObject.Find("Grid").GetComponent<SlotsGrid>();
			grid.OnSpinFinished += () => {
				spinning = false;
				if (currentResultCheck.Win){
					grid.HighlightTiles(currentResultCheck.WinningIndices);
					OnWin(currentResultCheck);
					Coins += (int) (Constants.BET_SIZE * currentResultCheck.BetMultiplier);
				}
			};
		}
		
		public void Spin(){
			if (!spinning){
				Coins -= Constants.BET_SIZE;
				spinning = true;
				int[][] endState = SpinResultService.ActiveService.GetNextFinalState();
				grid.Spin(endState);
				
				currentResultCheck = new WinStateCheck(endState);
			}
		}
	}

}