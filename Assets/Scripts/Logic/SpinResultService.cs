﻿using UnityEngine;
using System.Collections;

public class SpinResultService : MonoBehaviour
{
	static public SpinResultService ActiveService { get; private set; }

	void Awake ()
	{
		ActiveService = this;
	}
	
	int spinsForNextWin = 2;
	const int SPINS_PER_WIN = 3;
	
	int[][] createRandomState ()
	{
		int[][] state = new int[5][];
		
		for (int x = 0; x < 5; x ++) {
			state [x] = new int[3];
			for (int y = 0; y < 3; y ++) {
				state [x] [y] = Random.Range (0, 11);
			}
		}
		
		return state;
	}
	
	public int[][] GetNextFinalState ()
	{
		int[][] state = createRandomState ();
		spinsForNextWin --;
		if (spinsForNextWin == 0) {
			spinsForNextWin = SPINS_PER_WIN;
			setAsWin (state);
		}
		return state;
	}
	
	void setAsWin (int[][] state)
	{
		int streakIndex = Random.Range (0, 11);
		if (Random.Range (0, 3) == 0) {
			if (Random.Range(0, 2) == 0){
				state[0][0] = streakIndex;
				state[1][1] = streakIndex;
				state[2][2] = streakIndex;
				state[3][1] = streakIndex;
				state[4][0] = streakIndex;
			} 
			else{
				state[0][2] = streakIndex;
				state[1][1] = streakIndex;
				state[2][0] = streakIndex;
				state[3][1] = streakIndex;
				state[4][2] = streakIndex;
			}
		} 
		else {
			int y = Random.Range (0, 3);
			for (int x = 0; x < 5; x ++) {
				state [x] [y] = streakIndex;
			}
		}
	}
}
