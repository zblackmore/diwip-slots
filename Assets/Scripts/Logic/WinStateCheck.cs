﻿using UnityEngine;
using System.Collections;
 
public class WinStateCheck {
	public bool Win { get; private set; }
	public float BetMultiplier { get; private set; }
	public Vector2[] WinningIndices { get; private set; }
	
	int[][] result;
	
	public WinStateCheck(int[][] result){
		this.result = result; 
		checkRows();
		checkDiagonals();
	}
	
	void checkRows(){
		for (int y = 0; y < result[0].Length && Win == false; y ++){
			int firstXValue = result[0][y];
			bool rowIsUniform = true;
			for (int x = 0; x < result.Length; x++){
				if (result[x][y] != firstXValue){
					rowIsUniform = false;
					break;
				}
			}
			if (rowIsUniform){
				Win = true;	
				BetMultiplier = 5;
				WinningIndices = new Vector2[result.Length];
				for (int i = 0; i < result.Length; i ++){
					WinningIndices[i] = new Vector2(i, y);
				}
			}
		}
	}
	
	void checkDiagonals(){
		Vector2[] groupA = new Vector2[5] { new Vector2(0, 0), new Vector2(1, 1), new Vector2(2, 2), new Vector2(3, 1), new Vector2(4, 0) };
		Vector2[] groupB = new Vector2[5] { new Vector2(0, 2), new Vector2(1, 1), new Vector2(2, 0), new Vector2(3, 1), new Vector2(4, 2) };
		if (isGroupUniform(groupA)){
			WinningIndices = groupA;
			Win = true;
			BetMultiplier = 10;
		}
		else if (isGroupUniform(groupB)){
			WinningIndices = groupB;
			Win = true;
			BetMultiplier = 10;
		}
	}
	
	bool isGroupUniform(Vector2[] indices){
		int firstValue = result[(int)indices[0].x][(int)indices[0].y];
		for (int i = 1; i < indices.Length; i ++){
			if (result[(int)indices[i].x][(int)indices[i].y] != firstValue){
				return false;
			}
		}
		return true;
	}
}
