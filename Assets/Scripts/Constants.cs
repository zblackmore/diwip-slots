﻿using UnityEngine;
using System.Collections;

namespace DiwipSlots
{
	static public class Constants
	{
		public const int SPIN_TILES_COUNT = 40;
		public const int BET_SIZE = 10;
	}	
}