﻿using UnityEngine;
using System.Collections;

namespace DiwipSlots
{
	public class UI : MonoBehaviour
	{
	
		GameLogic logic;
	
		WinStateCheck currentWin;
		
		void Start ()
		{
			logic = GameObject.Find ("GameLogic").GetComponent<GameLogic> ();
			logic.OnWin += (win) => currentWin = win;
		}
	
		void OnGUI ()
		{
			if (!logic.Spinning){
				if (GUI.Button (new Rect (800, Screen.height * 0.5f - 30, 60, 60), "Spin") || Input.GetKeyUp(KeyCode.Space)) {
					logic.Spin();
					currentWin = null;
				}
			}
			if (currentWin != null){
				GUI.Label(new Rect(5, Screen.height * 0.5f - 30, 120, 60), "You won " + currentWin.BetMultiplier * Constants.BET_SIZE + " coins!"); 
			}
			GUI.Label(new Rect(10, Screen.height * 0.1f, 200, 60), "Coins: " + logic.Coins);
			GUI.Label(new Rect(10, Screen.height * 0.2f, 200, 60), "Bet Size: " + Constants.BET_SIZE);
		}
	}
}