﻿using UnityEngine;
using System.Collections;

public abstract class BaseTimer {
	
	public bool Finished { get; protected set; }
	public bool Repeating { get; private set; }
	
	public delegate void OnTimer(BaseTimer timer);
	protected OnTimer callback { private set; get; }
	
	public BaseTimer(OnTimer callback, bool repeating){
		this.callback = callback;
		Repeating = repeating;
	}
	
	public abstract void Update();
	public void Stop(){
		Finished = true;
	}
}
