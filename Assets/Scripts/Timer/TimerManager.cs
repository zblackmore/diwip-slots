﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class TimerManager : MonoBehaviour {
	
	List<BaseTimer> timers;
	
	public static TimerManager ActiveTimer { get; private set; }
	
	void Awake() {
		timers = new List<BaseTimer>();
		ActiveTimer = this;
	}
	
	void Update () {
		timers.ForEach(t => t.Update());
		timers.RemoveAll(t => t.Finished);
	}
	
	public Timer FireTimeTimer(float time, Timer.OnTimer callback){
		return FireTimeTimer(time, callback, false);
	}
	
	public Timer FireTimeTimer(float time, Timer.OnTimer callback, bool repeating){
		Timer newTimer = new Timer(time, callback, repeating);
		timers.Add(newTimer);
		return newTimer;
	}
}
