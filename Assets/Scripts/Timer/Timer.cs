using UnityEngine;
using System.Collections;

public class Timer : BaseTimer {
	float delta;
	float time;
	float initialTime;
	
	public Timer(float deltaTime, OnTimer callback) : this(deltaTime, callback, false){
		
	}
	
	public Timer(float deltaTime, OnTimer callback, bool repeating) : base(callback, repeating){
		delta = deltaTime;
		initialTime = time = delta;
		
	}
	
	public override void Update(){
		time -= Time.deltaTime;
		if (time <= 0 && !Finished){
			time = delta;
			callback(this);
			if (Repeating){
				time = initialTime;
			}
			else{
				Finished = true;
			}
		}
	}
}
